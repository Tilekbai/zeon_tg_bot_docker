import telebot, wikipedia, re
from config import *


bot = telebot.TeleBot(BOT_TOKEN)

wikipedia.set_lang("en")

def getwiki(s):
     try:
         ny = wikipedia.page(s)
         # Get the first thousand characters
         wikitext=ny.content[:1000]
         # Divide by points
         wikimas=wikitext.split('.')
         # Discard everything after the last dot
         wikimas = wikimas[:-1]
         # Create an empty variable for text
         wikitext2 = ''
         # Loop through the lines where there are no equal signs (i.e. everything except headings)
         for x in wikimas:
             if not('==' in x):
                     # If there are more than three characters left in the string, add it to our variable and return the dots lost when splitting the strings in place
                 if(len((x.strip()))>3):
                    wikitext2=wikitext2+x+'.'
             else:
                 break
         # Now, with the help of regular expressions, we remove the markup
         wikitext2=re.sub('\([^()]*\)', '', wikitext2)
         wikitext2=re.sub('\([^()]*\)', '', wikitext2)
         wikitext2=re.sub('\{[^\{\}]*\}', '', wikitext2)
         # Return a text string
         return wikitext2
     # Handle the exception that the wikipedia module might have returned when requested
     except Exception as e:
         return 'There is no information about this in the encyclopedia'
# Function that handles the /start command
@bot.message_handler(commands=["start"])
def start(m, res=False):
     bot.send_message(m.chat.id, "Send me any word and I'll look it up on Wikipedia")
# Receiving messages from the user
@bot.message_handler(content_types=["text"])
def handle_text(message):
     bot.send_message(message.chat.id, getwiki(message.text))
# Run the bot
bot.polling(none_stop=True, interval=0)