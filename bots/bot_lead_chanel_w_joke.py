import telebot
import time
from config import *


bot = telebot.TeleBot(BOT_TOKEN)
# Telegram channel address, starts with @
CHANNEL_NAME = CHANNEL_NAME
# Load the list of jokes
f = open('data/fun.txt', 'r', encoding='UTF-8')
jokes = f.read().split('\n')
f.close()
# Until the jokes run out, send them to the channel
for joke in jokes:
     bot.send_message(CHANNEL_NAME, joke)
     # Pause for one hour
     time.sleep(3600)
bot.send_message(CHANNEL_NAME, "Jokes are over :-(")