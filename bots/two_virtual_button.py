import telebot
import random
from telebot import types
from config import *


f = open('data/facts.txt', 'r', encoding='UTF-8')
facts = f.read().split('\n')
f.close()

f = open('data/thinks.txt', 'r', encoding='UTF-8')
thinks = f.read().split('\n')
f.close()

bot = telebot.TeleBot(BOT_TOKEN)

@bot.message_handler(commands=["start"])
def start(m, res=False):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("Fact")
    item2 = types.KeyboardButton("Saying")
    markup.add(item1)
    markup.add(item2)
    bot.send_message(m.chat.id, 'Press:\nFact for an interesting fact\nSaying - for a wise quote', reply_markup=markup)

@bot.message_handler(content_types=["text"])
def handle_text(message):
    answer = ""  # Initialize the variable with a default value
    if message.text.strip() == 'Fact':
        answer = random.choice(facts)
    elif message.text.strip() == 'Saying':
        answer = random.choice(thinks)
    bot.send_message(message.chat.id, answer)

bot.polling(none_stop=True, interval=0)
