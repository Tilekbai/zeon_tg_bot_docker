import telebot
import os
from fuzzywuzzy import fuzz
from config import *


bot = telebot.TeleBot(BOT_TOKEN)
# Load the list of phrases and answers into an array
mas=[]
if os.path.exists('data/boltun.txt'):
     f=open('data/boltun.txt', 'r', encoding='UTF-8')
     for x in f:
         if(len(x.strip()) > 2):
             mas.append(x.strip().lower())
     f.close()
# Using fuzzywuzzy, we calculate the most similar phrase and give the next element of the list as an answer
def answer(text):
     try:
         text=text.lower().strip()
         if os.path.exists('data/boltun.txt'):
             a = 0
             n=0
             nn = 0
             for q in mas:
                 if('u: ' in q):
                     # Use fuzzywuzzy to get how similar two strings are
                     aa=(fuzz.token_sort_ratio(q.replace('u: ',''), text))
                     if(aa > a and aa!= a):
                         a = aa
                         nn = n
                 n = n + 1
             s = mas[nn + 1]
             return s
         else:
             return 'Error'
     except:
         return 'Error'
# "Start" command
@bot.message_handler(commands=["start"])
def start(m, res=False):
         bot.send_message(m.chat.id, "I'm online. Text me Hello )")
# Receiving messages from the user
@bot.message_handler(content_types=["text"])
def handle_text(message):
     # Write logs
     f=open('data/' + str(message.chat.id) + '_log.txt', 'a', encoding='UTF-8')
     s=answer(message.text)
     f.write('u: ' + message.text + '\n' + s +'\n')
     f.close()
     # Sending a response
     bot.send_message(message.chat.id, s)
# Run the bot
bot.polling(none_stop=True, interval=0)