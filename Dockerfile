FROM python:3.9-alpine

COPY bots/config.py /bots/
COPY bots/echo_bot.py /bots/
COPY bots/bot_masha.py /bots/
COPY bots/bot_lead_chanel_w_joke.py /bots/
COPY bots/two_virtual_button.py /bots/
COPY bots/wiki_bot.py /bots/
COPY requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt

WORKDIR /bots
CMD ["python3", "wiki_bot.py"]