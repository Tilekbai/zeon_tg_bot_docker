# Инструкция по развертыванию проекта

Склонируйте репозиторий из Gitlab на свой локальный компьютер с помощью команды:
git clone https://gitlab.com/Tilekbai/zeon_tg_bot_docker.git

Перейдите в корневой каталог проекта.
Создайте виртуальное окружение, выполнив команду:
python3 -m venv venv

Активируйте виртуальное окружение, выполнив команду:
source venv/bin/activate

Установите необходимые зависимости, выполнив команду:
pip install -r requirements.txt

Создайте файл - bots/config.py
Внутрь файла добавьте следующие настройки:
	BOT_TOKEN = '************' добавьте свой токен телеграм бота
    CHANNEL_NAME = "@***********" добавьте название вашего канала после знака @
	
Прокатите миграции, выполнив команду:
python manage.py migrate

Для запуска отдельных скриптов:
python bots/<название скрипта>

Для запуска докер:
sudo docker build . -t zeon_telebot
sudo docker run -it -e KEY="YOUR_KEY" zeon_telebot